library('plyr')
source('~/networkregression/functions.R')

working.dir <- '~/Desktop/BBL/data/joy/BBL/studies/pnc/n1601_dataFreeze/'
setwd(working.dir)
demogra <- read.csv('./demographics/n1601_demographics_go1_20161212.csv')
hx_qa <- read.csv("./health/n1601_health_20161214.csv")
t1_qa <- read.csv("./neuroimaging/t1struct/n1601_t1QaData_20170306.csv")
mod_qa <- read.csv("./neuroimaging/rest/n1601_RestQAData_20170714.csv")
bifactor <- read.csv('./clinical/n1601_goassess_clinical_factor_scores_20161212.csv')

data.list <- list(demogra,hx_qa,t1_qa,mod_qa,bifactor)

sample_merge <- join_all(data.list,by = 'bblid', type = 'inner')
sample_merge_qa <- subset(sample_merge, restExclude ==0 & t1Exclude == 0 & healthExcludev2 == 0)

list.folds <- createFolds(sample_merge_qa$ageAtScan1, k = 3)
list.train <- sample_merge_qa[list.folds$Fold1,]
list.test <- sample_merge_qa[list.folds$Fold2,]
list.valid <- sample_merge_qa[list.folds$Fold3,]

compile_network <- function(subj.list, networktype){
  n_sample <- dim(subj.list)[1]
  if (networktype == "power") {
    sample_net<-array(NA, c(n_sample,264, 264))
    for (i in 1:n_sample){
      scanid <- subj.list$scanid[i]
      netpath<- paste("./neuroimaging/rest/restNetwork_264PowerPNC/264PowerPNCNetworks/",scanid,"_264PowerPNC_network.txt",sep="")
      sample_net[i,,] <- as.matrix(read.table(netpath))
      print(paste(i,"."," copying ",scanid,"_",networktype,sep=""))
    }
  }
  return(sample_net)
}

As.train <- compile_network(list.train,'power')
As.test <- compile_network(list.test,'power')
As.valid <- compile_network(list.valid,'power')

powernodes <- read.csv('./neuroimaging/rest/restNetwork_power/Consensus264.csv')
clean_adj <- function(adj.list){
  uncertain_nodes <- which(powernodes$Community==-1)
  adj.list.clean <- array(NA,dim = c(dim(adj.list)[1],236,236))
  for (i in 1:dim(adj.list)[1]){
    adj.list.clean[i,,] <- adj.list[i,-uncertain_nodes,-uncertain_nodes]
  }
  return(adj.list.clean)
  }
As.train <- clean_adj(As.train)
As.test <- clean_adj(As.test)
As.valid <- clean_adj(As.valid)

cov_of_interest <- c('ageAtScan1','sex','race2','restRelMeanRMSMotion',
                     'mood_4factor','psychosis_4factor','externalizing_4factor','phobias_4factor',
                     'overall_psychopathology_4factor')
cov_of_interest <- c('ageAtScan1','restRelMeanRMSMotion',
                     'overall_psychopathology_4factor')
Xs.train <- list.train[,cov_of_interest]
Xs.test <- list.test[,cov_of_interest]
Xs.valid <- list.valid[,cov_of_interest]

Ks <- powernodes$Community[-uncertain_nodes]

lam1s<-exp(seq(3,-5,len=9));lam2s<-exp(seq(3,-5,len=9));
out <- MultiScaleNetworkReg(As=As.train, X=Xs.train, Ks=Ks, lam1s=lam1s, lam2s=lam2s)

